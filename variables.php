<?php
$description = "Hi. This is my website. I created this website (blog) so that I can share all the things that I've learned, built and almost built but failed.";
$keywords = "jagannath,bhat";
$icon = "/logo.png";
$image = "og.jpg";
$title = "Jagannath Bhat";
$title2 = "Jagannath Bhat&#160;<small>-&#160;My Blog</small>";
$type = "website";
$url = "http://www.jagannathbhat.esy.es/";

$meta = array(
array("charset" => "UTF-8"),
array("http-equiv" => "X-UA-Compatible", ),
array("name" => "apple-mobile-web-app-capable", "content" => "yes"),
array("name" => "apple-mobile-web-app-status-bar-style", "content" => "black"),
array("name" => "apple-mobile-web-app-title", "content" => $title),
array("name" => "author", "content" => "Jagannath Bhat"),
array("name" => "description", "content" => $description),
array("name" => "keywords", "content" => $keywords),
array("name" => "mobile-web-app-capable", "content" => "yes"),
array("name" => "msapplication-TileColor", "content" => "#000000"),
array("name" => "msapplication-TileImage", "content" => $icon),
array("name" => "theme-color", "content" => "#2196F3"),
array("name" => "viewport", "content" => "width=device-width, initial-scale=1.0"),
array("property" => "og:description", "content" => $description),
array("property" => "og:image", "content" => $image),
array("property" => "og:rich_attachment", "content" => "true"),
array("property" => "og:title", "content" => $title),
array("property" => "og:type", "content" => $type),
array("property" => "og:url", "content" => $url)
);
?>