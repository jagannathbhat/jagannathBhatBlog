<?php include $urlAbsolute."/variables.php"; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="apple-touch-icon-precomposed" href="<?php echo $icon?>" />
		<link rel="icon" href="<?php echo $icon?>" sizes="192x192" />
		<link rel="shortcut icon" href="<?php echo $icon?>" />
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css">
		<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-pink.min.css" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
		<link rel="stylesheet" href="https://use.fontawesome.com/121ae719e0.css" />
		<link rel="stylesheet" href="http://use.fontawesome.com/releases/v4.6.3/css/font-awesome-css.min.css" />
<?php
for($i = 0; $i < count($meta); $i++) {
	echo "		<meta ";
	foreach($meta[$i] as $key => $value) {
		echo $key."=\"".$value."\" ";
	}
	echo "/>
";
}
?>
		<style>
<?php
include $_SERVER["DOCUMENT_ROOT"]."/style.css";
if(file_exists($urlAbsolute."/index.css")) {
	echo "\n";
	include $urlAbsolute."/index.css";
}
?>
		</style>
		<title><?php echo $title; ?></title>
	</head>
	<body>
		<div id="loading">
			<div id="innerLoading">
				<div class="mdl-spinner mdl-js-spinner is-active" style="display: inline-block"></div>
			</div>
		</div>
		<div class="mdl-js-layout mdl-layout mdl-layout--fixed-header">
			<header class="mdl-layout__header">
				<div class="mdl-layout__header-row">
					<div class="mdl-layout-spacer"></div>
					<nav class="mdl-layout--large-screen-only mdl-navigation">
	<?php include $_SERVER["DOCUMENT_ROOT"]."/links.php"; ?>
					</nav>
				</div>
			</header>
			<div class="mdl-layout__drawer">
				<nav class="mdl-navigation excludedElements">
	<?php include $_SERVER["DOCUMENT_ROOT"]."/links.php"; ?>
				</nav>
			</div>
			<main class="mdl-layout__content">
				<div class="page-content">
					<div class="mdl-grid" id="banner">
						<div class="mdl-cell mdl-cell--12-col mdl-cell--bottom">
							<h1><?php echo $title2; ?></h1>
						</div>
					</div>
<?php include $urlAbsolute."/content.php"; ?>
				</div>
				<footer class="mdl-mini-footer">
					<div class="mdl-mini-footer__left-section">
						<div class="mdl-logo">
							Created by Jagannath Bhat
						</div>
						<ul class="mdl-mini-footer__link-list">
							<li><a href="http://www.facebook.com/jagannathbhat1998/" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
							<li><a href="http://www.linkedin.com/in/jagannathbhat1998/" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
							<li><a href="http://plus.google.com/+jagannathbhat1998/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
							<!--li><a href="http://www.youtube.com/c/jagannathbhat1998/" target="_blank"><i class="fa fa-youtube-square"></i></a></li-->
						</ul>
					</div>
				</footer>
			</main>
		</div>
		<div id="fabButtons">
			<button class="mdl-button mdl-button--colored mdl-js-button mdl-button--fab mdl-js-ripple-effect" id="pageUpButton" title="Go to top">
				<i class="material-icons">keyboard_arrow_up</i>
			</button>
		</div>
		<div id="sideMenu"></div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="/jquery.touchSwipe.min.js"></script>
		<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		<script>
<?php
include $_SERVER["DOCUMENT_ROOT"]."/script.js";
if(file_exists($urlAbsolute."/index.js")) {
	echo "\n";
	include $urlAbsolute."/index.js";
}
?>
		</script>
	</body>
</html>