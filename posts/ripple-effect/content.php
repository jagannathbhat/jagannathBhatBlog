					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--4-col mdl-cell--top">
							<p>Advertisement</p>
						</div>
						<div class="mdl-cell mdl-cell--8-col mdl-cell--top">
							<h4>Brief Description</h4>
							<p><?php echo $description; ?></p>
							<a download href="index.html"><button class="mdl-button mdl-button--accent mdl-button--raised mdl-js-button mdl-js-ripple-effect">Download Code</button></a>&#160;&#160;&#160;<a href="https://repl.it/I3jp/4" target="_blank"><button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect">Demo <i class="material-icons">open_in_new</i></button></a>
							<h4>The Code</h4>
							<p>You can add ripple effect by simply adding the following code to your webpage.</p>
							<h5>HTML</h5>
							<div class="jb-code">
								<code>
									&#60;svg class="rippler" xmlns="http://www.w3.org/2000/svg"&#62;<br />
									&#160;&#160;&#160;&#60;circle cx="0" cy="0" r="0" fill="rgba(160, 160, 160, 0.1)" /&#62;<br />
									&#60;/svg&#62;<br />
								</code>
							</div>
							<h5>CSS</h5>
							<div class="jb-code">
								<code>
									.rippler {<br />
									&#160;&#160;&#160;height: 0px;<br />
									&#160;&#160;&#160;opacity: 1;<br />
									&#160;&#160;&#160;position: absolute;<br />
									&#160;&#160;&#160;width: 0px;<br />
									&#160;&#160;&#160;z-index: -1;<br />
									}<br />
								</code>
							</div>
							<h5>jQuery</h5>
							<p>You need to inlcude jquery in your page. To do that, add the following script before your main script.</p>
							<div class="jb-code">
								<code>
									&#60;script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"&#62;&#60;/script&#62;
								</code>
							</div><br />
							<p>Now add the following to your main script.</p>
							<div class="jb-code">
								<code>
									$(document).on("click", ".applyRipple", function(e) {
									&#160;&#160;&#160;var a;
									&#160;&#160;&#160;var b;
									&#160;&#160;&#160;var objectBorder = $(this).css("border-radius");<br />
									&#160;&#160;&#160;var objectHeight = $(this).outerHeight();<br />
									&#160;&#160;&#160;var objectPositionX = $(this).offset().left;<br />
									&#160;&#160;&#160;var objectPositionY = $(this).offset().top;<br />
									&#160;&#160;&#160;var objectWidth = $(this).outerWidth();<br />
									&#160;&#160;&#160;$(".rippler").css({"border-radius": objectBorder,"height": objectHeight + "px", "left": objectPositionX, "top": objectPositionY, "width": objectWidth + "px", "z-index": "0"});<br />
									&#160;&#160;&#160;$(".rippler circle").attr("cx", e.pageX - objectPositionX).attr("cy", e.pageY - objectPositionY).attr("fill", "rgba(0, 0, 0, 0.1)");<br />
									&#160;&#160;&#160;$(".rippler circle").animate({<br />
									&#160;&#160;&#160;&#160;&#160;&#160;"psuedo-property": "300%"<br />
									&#160;&#160;&#160;}, {<br />
									&#160;&#160;&#160;&#160;&#160;&#160;step: function(a, b) {<br />
									&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;$(".rippler circle").attr("r", a);<br />
									&#160;&#160;&#160;&#160;&#160;&#160;},<br />
									&#160;&#160;&#160;&#160;&#160;&#160;done: function(a, b) {<br />
									&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;$(".rippler").css({"height": "0px", "left": "0px", "top": "0px", "width": "0px", "z-index": "-1"});<br />
									&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;$(".rippler circle").attr("cx", "0").attr("cy", "0").attr("r", "0%").css("psuedo-property", "0%");<br />
									&#160;&#160;&#160;&#160;&#160;&#160;}<br />
									&#160;&#160;&#160;}, 400);<br />
									});
								</code>
							</div>
							<h5>That's it!</h5>
							<p>You just have to add the class <span class="jb-code" style="margin: 0px; padding: 4px;"><code>'applyRipple'</code></span> to any element on which you want the ripple effect to appear on click.</p>
							<h4>Leave your comments here</h4>
							<div id="commentForm">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="name" type="text">
									<label class="mdl-textfield__label" for="name">Name</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="email" type="text">
									<label class="mdl-textfield__label" for="email">Email</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield">
									<textarea class="mdl-textfield__input" id="comment" rows= "5" type="text"></textarea>
									<label class="mdl-textfield__label" for="comment">Comment</label>
								</div><br />
								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" id="submit">Submit</button><br />
								<p>All fields are required.</p>
							</div>
							<hr width="80%" />
							<h4>Comments</h4>
							<div id="comments">
							</div>
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									<center>
										<button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect" id="showMore" onclick="pourComments()">Show more</button>
										<h6 id="noMore" style="display: none;">No more comments</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
