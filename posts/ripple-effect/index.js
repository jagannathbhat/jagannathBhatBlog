			var lastComment = 0;
			var snackbarContainer = document.querySelector("#toast");
			
			$(document).ajaxComplete(function(event, xhr, settings) {
				if(settings.url === "../commentsCount.php?postid=" + <?php echo $row["id"]?> + "&lastComment=" + lastComment) {
					$("#loading").css("display", "none");
				}
			});
			
			$(document).on("click", "#submit", function() {
					var data = {message: ""};
					var fields = ["#name", "#email", "#comment"];
					var flag = 0;
					fields.forEach(function(item, index) {
						if($(item).prop("value") == "") {
							$(item).parent().addClass("is-invalid");
							flag = 1;
						}
					});
					if(flag == 1) {
						data.message = "All fields are required.";
					}
					if($("#name").prop("value") != "") {
						if(/^[a-zA-Z\s]+$/.test($("#name").prop("value")) == false) {
							if(data.message != "") {
								data.message += " ";
							}
							data.message += "Invalid Name.";
							flag = 1;
						}
					}
					if($("#email").prop("value") != "") {
						if(/^[a-zA-Z\s\d]+\@+.+$/.test($("#email").prop("value")) == false) {
							if(data.message != "") {
								data.message += " ";
							}
							data.message += "Invalid email.";
							flag = 1;
						}
					}
					if(flag == 0) {
						var content = $("<div></div>");
						content.load("../addComment.php?postid=" + <?php echo $row["id"]?> + "&name=" + encodeURI($("#name").prop("value")) + "&email=" + encodeURI($("#email").prop("value")) + "&comment=" + encodeURI($("#comment").prop("value")));
						$("#comment").prop("value", "");
						$("#email").prop("value", "");
						$("#name").prop("value", "");
						data.message = "Comment Submitted. Thank you!";
						$("#comments").html("");
						lastComment = 0;
						pourComments();
					}
					snackbarContainer.MaterialSnackbar.showSnackbar(data);
			});
			
			$(document).on("change", "#email", function() {
				if(/^[a-zA-Z\s\d]+\@+.+$/.test($("#email").prop("value")) == false) {
					$("#email").parent().addClass("is-invalid");
					$("#email").next().text("Name (Invalid email)");
				}
			});
			
			$(document).on("change", "#name", function() {
				if(/^[a-zA-Z\s]+$/.test($("#name").prop("value")) == false) {
					$("#name").parent().addClass("is-invalid");
					$("#name").next().text("Name (Invalid name)");
				}
			});
			
			$(document).ready(function() {
				pourComments();
			});
			
			function pourComments() {
				$("#loading").css("display", "table");
				content = $("<div></div>");
				content.load("../pourComments.php?postid=" + <?php echo $row["id"]?> + "&lastComment=" + lastComment, function(responseText, textStatus, jqXHR) {
					if(responseText == "") {
						$("#noMore").text("No comments");
					}
				});
				$("#comments").append(content);
				lastComment += 5;
				content = $("<div class=\"mdl-grid\"></div>");
				content.load("../commentsCount.php?postid=" + <?php echo $row["id"]?> + "&lastComment=" + lastComment, "", function(responseText, textStatus, jqXHR) {
					if(responseText == "Not okay") {
						$("#showMore").removeAttr("onclick").hide(400);
						$("#noMore").show();
					}
				});
			}