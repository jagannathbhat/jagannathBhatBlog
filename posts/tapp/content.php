					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--4-col mdl-cell--top">
							<p>Advertisement</p>
						</div>
						<div class="mdl-cell mdl-cell--8-col mdl-cell--top">
							<h4>Brief Description</h4>
							<p><?php echo $description; ?></p>
							<a download href="tapp.cpp"><button class="mdl-button mdl-button--accent mdl-button--raised mdl-js-button mdl-js-ripple-effect">Download Code</button></a>
							<br />
							<p>Note: It requires Turbo C++</p>
							<h4>The Story</h4>
							<p>This was my high school computer project. I was asked to make a program using C++, a programming language I had just learned. It was taught to me by Mrs. Yasmin Shankar. I had to make a program that had some real-life application.</p>
							<p>At first I just made a program that let any user store records of students in a common file. It had functionalities such as add student record, modify student record, delete student record and search & display student record. But after making the program I still had time. So I started thinking on how I could make it better.</p>
							<p>The program had to be useful in real life. If any user could manipulate all the data stored, it wouldn’t be secure. The student records created by one user can be modified by another. So changed the program and made it such that a user would have to login to his/her account in order to store or manipulate student records. Each user’s data was stored in different files rather than a single file for all users. Hence, the data of one user cannot be manipulated by another.</p>
							<p>This was the first time I made a program in which I had to check for errors in the output as well. Until then, I made simple programs like program to find factorial of a number, check if a number is prime or not, etc. In such programs, the output was only a bunch of numbers or strings. Nothing to worry about there, just make sure the output does show up on the screen or gets stored in a file. But here, I had to make the output look good and make a more user-friendly interface. It is something we programmers like to call front-end development. So, I had to check the program’s output again and again for different inputs. And the final program was great!</p>
							<p>I think every programmer must test his/her program or app or whatever and find out any flaw. I also asked teachers to use it and I used their feedbacks as well. I guess that is why beta apps are made. When some people use the beta app and give you their feedback, you can make necessary changes to the app. So, in the end, when you’re app is made available to everyone, there will be very few errors and the app will be everything your end user expects because an end-user already used your app and gave you his/her feedback.</p>
							<h4>Leave your comments here</h4>
							<div id="commentForm">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="name" type="text">
									<label class="mdl-textfield__label" for="name">Name</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="email" type="text">
									<label class="mdl-textfield__label" for="email">Email</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield">
									<textarea class="mdl-textfield__input" id="comment" rows= "5" type="text"></textarea>
									<label class="mdl-textfield__label" for="comment">Comment</label>
								</div><br />
								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" id="submit">Submit</button><br />
								<p>All fields are required.</p>
							</div>
							<hr width="80%" />
							<h4>Comments</h4>
							<div id="comments">
							</div>
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									<center>
										<button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect" id="showMore" onclick="pourComments()">Show more</button>
										<h6 id="noMore" style="display: none;">No more comments</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
