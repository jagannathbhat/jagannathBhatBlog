					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--4-col mdl-cell--top">
							<p>Advertisement</p>
						</div>
						<div class="mdl-cell mdl-cell--8-col mdl-cell--top">
							<h4>Brief Description</h4>
							<p><?php echo $description; ?></p>
							<a download href="cenarea.zip"><button class="mdl-button mdl-button--accent mdl-button--raised mdl-js-button mdl-js-ripple-effect">Download Executable</button></a>&#160;&#160;&#160;<a download href="cenarea.py"><button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect">Download Code</button></a>&#160;&#160;&#160;<a href="https://repl.it/I1lT/0" target="_blank"><button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect">Demo <i class="material-icons">open_in_new</i></button></a>
							<h4>The Story</h4>
							<p>This was a project given to me during my first semester while I was doing my Bachelor of Technology in Computer Science. I was just introduced to a new language “Python”. An incredibly flexible and powerful language. It was taught to me by Dr. Manish TI. He asked me to make a program, using the new language I learned, which would do some computation which is helpful in some other subject that I was learning in that semester. After Computer programming, Mechanics was my favorite subject from semester 1. Hence, I decided to make Cenarea.</p>
							<p>I am not completely satisfied with this program because it is not that easy for users to use this program. Especially since they have to do some calculation themselves in order to give meaningful input to the program.</p>
							<p>So my advice to you is that, when making programs or apps, make sure that the users don’t have to do any work. Instead, your program or app must be able to take the raw input from the users and do all the work for them.</p>
							<h4>Leave your comments here</h4>
							<div id="commentForm">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="name" type="text">
									<label class="mdl-textfield__label" for="name">Name</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="email" type="text">
									<label class="mdl-textfield__label" for="email">Email</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield">
									<textarea class="mdl-textfield__input" id="comment" rows= "5" type="text"></textarea>
									<label class="mdl-textfield__label" for="comment">Comment</label>
								</div><br />
								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" id="submit">Submit</button><br />
								<p>All fields are required.</p>
							</div>
							<hr width="80%" />
							<h4>Comments</h4>
							<div id="comments">
							</div>
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									<center>
										<button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect" id="showMore" onclick="pourComments()">Show more</button>
										<h6 id="noMore" style="display: none;">No more comments</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
