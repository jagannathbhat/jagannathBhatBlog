<?php
include $_SERVER["DOCUMENT_ROOT"]."/databaseInfo.php";
$sql = "SELECT * FROM posts WHERE url = \"".implode("", explode($_SERVER["DOCUMENT_ROOT"], implode("/", explode("\\", dirname(__FILE__)))))."/"."\"";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$article["author"] = "https://www.facebook.com/jagannathbhat1998";
$article["tag"] = $row["labels"];
$article["time"] = $row["date"];
$description = $row["description"];
$keywords = $row["keywords"];
$icon = "/logo.png";
$image = "";
$title = $row["name"]." | Jagannath Bhat";
$title2 = $row["name"];
$url = "http://www.jagannathbhat.esy.es".$row["url"];

$meta = array(
array("charset" => "UTF-8"),
array("http-equiv" => "X-UA-Compatible", ),
array("name" => "apple-mobile-web-app-capable", "content" => "yes"),
array("name" => "apple-mobile-web-app-status-bar-style", "content" => "black"),
array("name" => "apple-mobile-web-app-title", "content" => $title),
array("name" => "author", "content" => "Jagannath Bhat"),
array("name" => "description", "content" => $description),
array("name" => "keywords", "content" => $keywords),
array("name" => "mobile-web-app-capable", "content" => "yes"),
array("name" => "msapplication-TileColor", "content" => "#000000"),
array("name" => "msapplication-TileImage", "content" => $icon),
array("name" => "theme-color", "content" => "#2196F3"),
array("name" => "viewport", "content" => "width=device-width, initial-scale=1.0"),
array("property" => "article:author", "content" => $article["author"]),
array("property" => "article:published_time", "content" => $article["time"]),
array("property" => "article:tag", "content" => $article["tag"]),
array("property" => "og:description", "content" => $description),
array("property" => "og:image", "content" => $image),
array("property" => "og:rich_attachment", "content" => "true"),
array("property" => "og:title", "content" => $title),
array("property" => "og:type", "content" => "article"),
array("property" => "og:url", "content" => $url)
);
?>