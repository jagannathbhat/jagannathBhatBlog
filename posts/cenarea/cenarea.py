#Program to find Centeroid and Moment of Inertia of a symmetrical complex figure
from math import pi

#xBar stores the x coordinates of centeroids
#yBar stores the y coordinates of centeroids
#Iy stores the areas
#Ix stores the inertias w.r.t. x centeroidal axes
#Iy stores the inertias w.r.t. y centeroidal axes
Areas = []
xBar = []
yBar = []
Ix = []
Iy = []

#Function to append parameters to xBar,yBar
def cenApp(a, b, c, d, e):
	key = 1
	while (key):
		ch = int(raw_input("Enter 1 to add the region to the total area\nOr 2 to remove the region from total area\n"))
		if ch == 1:
			Areas.append(a)
			xBar.append(a * b)
			yBar.append(a * c)
			Ix.append(d)
			Iy.append(e)
			key = 0
		elif ch == 2:
			Areas.append(-1 * a)
			xBar.append(-1 * a * b)
			yBar.append(-1 * a * c)
			Ix.append(-1 * d)
			Iy.append(-1 * e)
			key = 0
		else:
			print "Invalid input. Try again."

#Function to find centeroid, area and inertia of rectangles
def Rect():
	xC, yC, Width, Height = float(raw_input("Enter coordinates of bottom left point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter width: ")), float(raw_input("Enter height: "))
	Area = Height * Width
	xC += Width / 2
	yC += Height / 2
	xI = (Width * Height ** 3) / 12
	yI = (Height * Width ** 3) / 12
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of circle
def Circ():
	xC, yC, rad = float(raw_input("Enter coordinates of centre point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the circle: "))
	Area = pi * rad ** 2
	xI = (pi * (rad * 2) ** 3) / 64
	yI = (pi * (rad * 2) ** 3) / 64
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of right angled triangle with hypoteneus on top left
def Tri1():
	xC, yC, Base, Height = float(raw_input("Enter coordinates of bottom left point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter base length: ")), float(raw_input("Enter height: "))
	Area = 0.5 * Height * Base
	xC += Base / 3
	yC += Height / 3
	xI = (Base * Height ** 3) / 36
	yI = (Height * Base ** 3) / 36
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of right angled triangle with hypoteneus on top right
def Tri2():
	xC, yC, Base, Height = float(raw_input("Enter coordinates of bottom right point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter base length: ")), float(raw_input("Enter height: "))
	Area = 0.5 * Height * Base
	xC -= Base / 3
	yC += Height / 3
	xI = (Base * Height ** 3) / 36
	yI = (Height * Base ** 3) / 36
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of right angled triangle with hypoteneus on bottom left
def Tri3():
	xC, yC, Base, Height = float(raw_input("Enter coordinates of top left point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter base length: ")), float(raw_input("Enter height: "))
	Area = 0.5 * Height * Base
	xC += Base / 3
	yC -= Height / 3
	xI = (Base * Height ** 3) / 36
	yI = (Height * Base ** 3) / 36
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of right angled triangle with hypoteneus on bottom right
def Tri4():
	xC, yC, Base, Height = float(raw_input("Enter coordinates of top right point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter base length: ")), float(raw_input("Enter height: "))
	Area = 0.5 * Height * Base
	xC -= Base / 3
	yC -= Height / 3
	xI = (Base * Height ** 3) / 36
	yI = (Height * Base ** 3) / 36
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Semi-circle with curve on top
def semiCirc1():
	xC, yC, rad = float(raw_input("Enter coordinates of centre point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	yC += ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 128
	yI = (pi * (rad * 2) ** 3) / 128
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Semi-circle with curve on bottom
def semiCirc2():
	xC, yC, rad = float(raw_input("Enter coordinates of centre point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	yC -= ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 128
	yI = (pi * (rad * 2) ** 3) / 128
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Semi-circle with curve on right
def semiCirc3():
	xC, yC, rad = float(raw_input("Enter coordinates of centre point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	xC += ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 128
	yI = (pi * (rad * 2) ** 3) / 128
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Semi-circle with curve on left
def semiCirc4():
	xC, yC, rad = float(raw_input("Enter coordinates of centre point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	xC -= ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 128
	yI = (pi * (rad * 2) ** 3) / 128
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Quadrant with curve on top right
def Quad1():
	xC, yC, rad = float(raw_input("Enter coordinates of corner point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	xC += ((4 * rad ) / (3 * pi))
	yC += ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 256
	yI = (pi * (rad * 2) ** 3) / 256
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Quadrant with curve on top left
def Quad2():
	xC, yC, rad = float(raw_input("Enter coordinates of corner point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	xC -= ((4 * rad ) / (3 * pi))
	yC += ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 256
	yI = (pi * (rad * 2) ** 3) / 256
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Quadrant with curve on bottom right
def Quad3():
	xC, yC, rad = float(raw_input("Enter coordinates of corner point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	xC += ((4 * rad ) / (3 * pi))
	yC -= ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 256
	yI = (pi * (rad * 2) ** 3) / 256
	cenApp(Area, xC, yC, xI, yI)

#Function to find centeroid and area of Quadrant with curve on bottom left
def Quad4():
	xC, yC, rad = float(raw_input("Enter coordinates of corner point\nx: ")), float(raw_input("y: ")), float(raw_input("Enter radius of the arc: "))
	Area = 0.5 * pi * rad ** 2
	xC -= ((4 * rad ) / (3 * pi))
	yC -= ((4 * rad ) / (3 * pi))
	xI = (pi * (rad * 2) ** 3) / 256
	yI = (pi * (rad * 2) ** 3) / 256
	cenApp(Area, xC, yC, xI, yI)

#Function to calculate centeroid and Inertia of entire region 
def calculate():
	sum1 = 0
	sum2 = 0
	for i in range(0, len(xBar)):
		sum1 += xBar[i]
		sum2 += Areas[i]
	try:
		Cx = sum1 / sum2
	except ZeroDivisionError:
		print "Cannot calculate centeroid."
		return
	sum1 = 0
	sum2 = 0
	for i in range(0, len(yBar)):
		sum1 += yBar[i]
		sum2 += Areas[i]
	try:
		Cy = sum1 / sum2
	except ZeroDivisionError:
		print "Cannot calculate centeroid."
		return
	print "Centeroid: (",  "%.2f" % Cx, ", ", "%.2f" % Cy, ")"
	sum1 = 0
	sum2 = 0
	for i in range(0, len(Ix)):
		sum1 += Ix[i] + (Areas[i] * ((Cy - yBar[i]) ** 2))
		sum2 += Iy[i] + (Areas[i] * ((Cx - xBar[i]) ** 2))
	print "Inertia w.r.t. centeroidal x axis: ", "%.2f" % sum1
	print "Inertia w.r.t. centeroidal y axis: ", "%.2f" % sum2
def clearData():
	global xBar
	global yBar
	global Areas
	global Ix
	global Iy
	xBar = []
	yBar = []
	Areas = []
	Ix = []
	Iy = []
	print "Data is cleared."
def about():
	print "Cenarea\nby Jagannath V Bhat\nThis is a program which can be used to calculate the centeroid and second moment of area of symmetrical composite areas that can be expressed as a combination triangles, rectangles, circles. Select the regions that your figure consists of and enter the requiered parameters.\n\n"
def exitMenu():
	global key
	key = 0
Options = {1 : Rect, 2 : Circ, 3 : Tri1, 4 : Tri2, 5 : Tri3, 6 : Tri4, 7 : semiCirc1, 8 : semiCirc2, 9 : semiCirc3, 10 : semiCirc4, 11 : Quad1, 12 : Quad2, 13 : Quad3, 14 : Quad4, 15: calculate, 16 : clearData, 17 : about, 18 : exitMenu}
key = 1
while (key):
	ch = int(raw_input("Menu\n01. Rectangle\n02. Circle\n03. Right angled triangle - hypoteneus at top right side\n04. Right angled triangle - hypoteneus at top left side\n05. Right angled triangle - hypoteneus at bottom right side\n06. Right angled triangle - hypoteneus at bottom left side\n07. Semi-circle - curve on the top\n08. Semi-circle - curve on the bottom\n09. Semi-circle - curve on the right\n10. Semi-circle - curve on left\n11. Quadrant - top right\n12. Quadrant - top left\n13. Quadrant - bottom right\n14. Quadrant - bottom left\n\n15. Calculate\n16. Clear Data\n17. About\n18. Exit\n\nEnter command: "))
	print "\n"
	if ch <= 0 or ch >= len(Options) + 1:
		print "Invalid option, try again."
	else:
		Options[ch]()
	raw_input("Press enter key to proceed.")
	print "\n\n\n"