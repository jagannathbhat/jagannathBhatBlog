					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--4-col mdl-cell--top">
							<p>Advertisement</p>
						</div>
						<div class="mdl-cell mdl-cell--8-col mdl-cell--top">
							<h4>Brief Description</h4>
							<p><?php echo $description; ?></p>
							<a href="http://www.excelenglish.esy.es/" target="_blank"><button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect">Visit Website <i class="material-icons">open_in_new</i></button></a>
							<h4>The Story</h4>
							<p>My best friend Sid said his mother wanted a website for her English tuition center. She was planning on putting up an ad and she wanted a website to show her contact details. She also asked me to put a few contents like what the people would gain from her classes and her qualifications for the job.</p>
							<p>So analyzed my website goals.
								<ul>
									<li>The main goal of the website was to display the contact in an easy-to-find manner.</li>
									<li>The people should find it very easy to contact the client.</li>
									<li>Show the content the client gave me, without compromising the main goal.</li>
									<li>The page should be quick to load.</li>
								</ul>
							</p>
							<p>Hence, I made a website with the following features.
								<ul>
									<li>A simple and clean interface which is easy to load. I also made the interface responsive. A responsive design looks good on screens of all sizes. This is important because the customers may be viewing the website using a large screen like a desktop or small one like a smartphone. No matter what the screen size, the design should look good and meet the same requirements.</li>
									<li>The contact details are the first thing seen on the website. Followed by the other content. This makes things easy for the customers who don’t want to go through the content and want to contact the client immediately. And of course, contact details are also given after the secondary content.</li>
									<li>A form which customers can easily fill to contact the client. This would be the easiest way because,
										<ol>
											<li>Customers don’t have to pick up the phone to dial</li>
											<li>Customers don’t have to compose an email, which is also time-consuming.</li>
										</ol>
									</li>
								</ul>
							</p>
							<p>Most web developers usually skip the “Analysing website goals” part and head straight to designing or developing. This step is important. It helps you focus on that one main goal. In my case it was to provide the contact details and make it easy for the customers to contact the client. It helps you construct the best design that meets all the client’s requirement. Designing is not always just about amazing people with cool graphics or awesome animations. It can also have goals like “Get people to buy a product”, “Make the online learning experience easy”, etc., where graphics and animations may not have a big role. When you want to make people to buy a product sold on a website, you should make sure that the best features of the product and the “Buy Now” button is highlighted. In such cases, animations can be a distraction unless applied at at the right place.</p>
							<h4>Leave your comments here</h4>
							<div id="commentForm">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="name" type="text">
									<label class="mdl-textfield__label" for="name">Name</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="email" type="text">
									<label class="mdl-textfield__label" for="email">Email</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield">
									<textarea class="mdl-textfield__input" id="comment" rows= "5" type="text"></textarea>
									<label class="mdl-textfield__label" for="comment">Comment</label>
								</div><br />
								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" id="submit">Submit</button><br />
								<p>All fields are required.</p>
							</div>
							<hr width="80%" />
							<h4>Comments</h4>
							<div id="comments">
							</div>
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									<center>
										<button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect" id="showMore" onclick="pourComments()">Show more</button>
										<h6 id="noMore" style="display: none;">No more comments</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
					<div id="toast" class="mdl-js-snackbar mdl-snackbar">
						<div class="mdl-snackbar__text"></div>
						<button class="mdl-snackbar__action" type="button"></button>
					</div>
