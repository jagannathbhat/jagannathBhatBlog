					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--4-col mdl-cell--top">
							<p>Advertisement</p>
						</div>
						<div class="mdl-cell mdl-cell--8-col mdl-cell--top">
							<h4>Brief Description</h4>
							<p><?php echo $description; ?></p>
							<a download href="ticTacToeMin.c"><button class="mdl-button mdl-button--accent mdl-button--raised mdl-js-button mdl-js-ripple-effect">Download Code</button></a>&#160;&#160;&#160;<a href="https://repl.it/I1dF/0" target="_blank"><button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect">Demo <i class="material-icons">open_in_new</i></button></a>
							<h4>Leave your comments here</h4>
							<div id="commentForm">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="name" type="text">
									<label class="mdl-textfield__label" for="name">Name</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input class="mdl-textfield__input" id="email" type="text">
									<label class="mdl-textfield__label" for="email">Email</label>
								</div><br />
								<div class="mdl-textfield mdl-js-textfield">
									<textarea class="mdl-textfield__input" id="comment" rows= "5" type="text"></textarea>
									<label class="mdl-textfield__label" for="comment">Comment</label>
								</div><br />
								<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" id="submit">Submit</button><br />
								<p>All fields are required.</p>
							</div>
							<hr width="80%" />
							<h4>Comments</h4>
							<div id="comments">
							</div>
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									<center>
										<button class="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect" id="showMore" onclick="pourComments()">Show more</button>
										<h6 id="noMore" style="display: none;">No more comments</h6>
									</center>
								</div>
							</div>
						</div>
					</div>
