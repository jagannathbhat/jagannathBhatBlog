/*
Tic Tac Toe Min
No rights reserved
*/

/*
Library Files
stdio.h - for input and output operations
*/
#include<stdio.h>

/*
This matrix stores "X"s and "O"s marked during the game.
It is made up of integer values 0, 1 or 2.
0 means that cell is marked with X
1 means that cell is marked with O
2 means that cell is not marked
*/
int grid[3][3];

/*
This array returns the mark of a player.
If a function accesses the mark of player 1 (i.e., player[0]), "X" is returned.
If a function accesses the mark of player 2 (i.e., player[1]), "O" is returned.
*/
char playerMarks[2] = "XO";

/*
Main Function
Function that controls the game.
*/
void main() {
	int column, flag = 0, gameOver = 0, i, j, num = 1, result, row;
	char unused;

	int checkGrid();
	void displayGrid();
	
	num = 0;

	for(i = 0; i < 3; i++) {
		for(j = 0; j < 3; j++) {
			grid[i][j] = 2;
		}
	}
	
	/*
	Displays the grid, asks players for where their mark should be made
	This is repeated until a player wins or the game is tied
	*/
	do {
		if(flag == 0) {
			displayGrid();
			printf("\n\n");
		}

		flag = 0;

		printf("Player %d\nEnter row and column number\nEnter 9 to exit\nRow: ", num + 1);
		scanf("%d", &row);
		unused = getchar();

		if(row == 9) {
			return;
		}
		else if(row < 0 || row > 2) {
			flag = 1;
			printf("Invalid row number.\n\n");
		}
		else {
			printf("Column: ");
			scanf("%d", &column);
			unused = getchar();
			
			if(column == 9) {
				return;
			}
			else if(column < 0 || column > 2) {
				flag = 1;
				printf("Invalid column number.\n\n");
			}
			else {
				if(grid[row][column] != 2) {
					flag = 1;
					printf("Cell is already marked. Choose another cell.\n\n");
				}
				else {
					grid[row][column] = num;
					result = checkGrid();
					if(result < 2) {
						gameOver = result + 1;
					}
					else {
						num = (num + 1) % 2;
					}
				}
			}
		}
	printf("Press enter to continue.\n");
	unused = getchar();
	printf("**************************************\n");
	} while(gameOver == 0);
	
	printf("Game Over\n");
	
	if(gameOver == 2) {
		printf("It's a tie.\n");
	}
	
	else {
		displayGrid();
		printf("Player %d won.\n", num + 1);
	}
}

/*
Function that checks the grid and returns an integer indicating what it found.
0 - Presently playing player won
1 - Game is tied
7 - Nothing to report
*/
int checkGrid() {
	int flag = 0, i, j;

	/*
	Checks if the player won
	*/
	for(i = 0; i < 3; i++) {
		if(grid[i][0] == grid[i][1]) {
			if(grid[i][0] == grid[i][2]) {
				if(grid[i][0] == 0 || grid[i][0] == 1) {
					return 0;
				}
			}
		}
		else if(grid[0][i] == grid[1][i]) {
			if(grid[0][i] == grid[2][i]) {
				if(grid[0][i] == 0 || grid[0][i] == 1) {
					return 0;
				}
			}
		}
	}
	if(grid[0][0] == grid[1][1]) {
		if(grid[0][0] == grid[2][2]) {
			if(grid[0][0] == 0 || grid[0][0] == 1) {
				return 0;
			}
		}
	}
	else if(grid[0][2] == grid[1][1]) {
		if(grid[0][2] == grid[2][0]) {
			if(grid[0][2] == 0 || grid[0][2] == 1) {
				return 0;
			}
		}
	}
	
	/*
	Checks if all cells are marked. In that case, the game is a tie.
	*/
	for(i = 0; i < 3; i++) {
		for(j = 0; j < 3; j++) {
			if(grid[i][j] == 2) {
				flag = 1;
			}
		}
	}
	
	if(flag == 0) {
		return 1;
	}
	
	return 7;
}

/*
Function to display the grid
*/
void displayGrid() {
	int i, j;
	for(i = 0; i < 3; i++) {
		for(j = 0; j < 3; j++) {
			if(grid[i][j] == 2) {
				printf("_\t");
			}
			else {
				printf("%c\t", playerMarks[grid[i][j]]);
			}
		}
		
		printf("\n");
	}
}
