<?php
include $_SERVER["DOCUMENT_ROOT"]."/databaseInfo.php";
$lastPost = 0;
if(isset($_GET["lastPost"])) {
	$lastPost = $_GET["lastPost"];
}
$sql = "SELECT * FROM posts";
$result = $conn->query($sql);
if($lastPost < $result->num_rows) {
	$sql = "SELECT * FROM posts ORDER BY id DESC LIMIT $lastPost, 5";
	$result = $conn->query($sql);
	$sql2 = "SELECT DATE_FORMAT(date,'%h:%i %p, %c %b %y') AS date FROM posts ORDER BY id DESC LIMIT $lastPost, 5";
	$result2 = $conn->query($sql2);
	for($i = 0; $row = $result->fetch_assoc(); $i++) {
		$row2 = $result2->fetch_assoc();
		$style="";
		if(file_exists(implode("/", explode("\\", dirname(__FILE__))).$row["url"]."og.jpg")) {
			$style = " style=\"background-image: url('".$row["url"]."og.jpg');\"";
		}
		echo "							<div class=\"mdl-cell mdl-cell--4-col\">
								<div class=\"jb-card-wide mdl-card mdl-shadow--2dp\">
									<div class=\"mdl-card__title\"".$style.">
										<div class=\"mdl-card__title-text\">
											<h2>".$row["name"]."</h2>
											<p>".$row2["date"]."</p>
										</div>
									</div>
									<div class=\"mdl-card__supporting-text\">".$row["description"]."<br /><br />
										<br /><br /><i class=\"material-icons\" style=\"font-size: 18px; vertical-align: bottom; width: 26px;\">labels</i>".$row["labels"]."</div>
									<div class=\"mdl-card__actions mdl-card--border\">
										<a class=\"mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect\" href=\"".$row["url"]."\">".$row["buttonText"]."</a>
";
		if($row["url2"] != "") {
			echo "										<a class=\"mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect\" href=\"".$row["url2"]."\"";
			if((strpos($row["url2"], "http") !== FALSE) || ($row["buttonText2"] == "View App")) {
				echo " target=\"_blank\"";
			}
			echo ">".$row["buttonText2"];
			if((strpos($row["url2"], "http") !== FALSE) || ($row["buttonText2"] == "View App")) {
				echo " <i class=\"material-icons\">open_in_new</i>";
			}
			echo "</a>
";
		}
		echo "									</div>
								</div>
							</div>
";
		if($i == 3) {
			
			echo "							<div class=\"mdl-cell mdl-cell--4-col\">
								<div class=\"jb-card-wide mdl-card mdl-shadow--2dp\">
									<div class=\"mdl-card__supporting-text\"><p>Advertisement</p></div>
								</div>
							</div>
";
		}
	}
}
$conn->close();
?>