			var lastPost = 0;
			
			$(document).ready(function() {
				pourPosts();
			});
			
			$(document).ajaxComplete(function(event, xhr, settings) {
				if(settings.url === "postsCount.php?lastPost=" + lastPost) {
					$("#loading").css("display", "none");
				}
			});
			
			function pourPosts() {
				$("#loading").css("display", "table");
				content = $("<div class=\"mdl-grid\"></div>");
				content.load("posts.php?lastPost=" + lastPost);
				$("#posts").append(content);
				lastPost += 5;
				content = $("<div class=\"mdl-grid\"></div>");
				content.load("postsCount.php?lastPost=" + lastPost, "", function(responseText, textStatus, jqXHR) {
					if(responseText == "Not okay") {
						$("#showMore").removeAttr("onclick").hide(400);
						$("#noMore").show();
					}
				});
			}
