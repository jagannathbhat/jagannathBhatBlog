<?php
$description = "I am Jagannath Bhat. I am a hardcore Linkin Park fan, an engineering student, a nerd. I don't actually own anything but a laptop and an old Android phone, and I intend to make the most out of them. I really love making things, like websites for example. I have a thirst for knowledge. I love learning about all sorts stuff. I am particularly interested in studying computer programming and music. So, I created this website (blog) so that I can share all the things that I've learned, built and almost built but failed. I am not ashamed of my failures. My failures are proof that I am trying and in my failures I learn a lot about myself, my flaws and what I should do better the next time. Hopefully, even you might take away some lessons from my failures. But I do encourage you to go out there and make some mistakes of your own and tell me about them.";
$keywords = "jagannath,bhat,about";
$icon = "/logo.png";
$image = "og.jpg";
$title = "About | Jagannath Bhat";
$title2 = "About me";
$type = "website";
$url = "http://www.jagannathbhat.esy.es/about/";

$meta = array(
array("charset" => "UTF-8"),
array("http-equiv" => "X-UA-Compatible", ),
array("name" => "apple-mobile-web-app-capable", "content" => "yes"),
array("name" => "apple-mobile-web-app-status-bar-style", "content" => "black"),
array("name" => "apple-mobile-web-app-title", "content" => $title),
array("name" => "author", "content" => "Jagannath Bhat"),
array("name" => "description", "content" => $description),
array("name" => "keywords", "content" => $keywords),
array("name" => "mobile-web-app-capable", "content" => "yes"),
array("name" => "msapplication-TileColor", "content" => "#000000"),
array("name" => "msapplication-TileImage", "content" => $icon),
array("name" => "theme-color", "content" => "#2196F3"),
array("name" => "viewport", "content" => "width=device-width, initial-scale=1.0"),
array("property" => "og:description", "content" => $description),
array("property" => "og:image", "content" => $image),
array("property" => "og:rich_attachment", "content" => "true"),
array("property" => "og:title", "content" => $title),
array("property" => "og:type", "content" => $type),
array("property" => "og:url", "content" => $url)
);
?>