			var documentClick_flag0 = 0;
			var documentClick_flag10 = 0;
			var pageUpButton_flag = 0;
			
			window.onload = function() {
				$("#loading").css("display", "none");
			};
			
			$(document).on("click", "#pageUpButton", function() {
				$(".mdl-layout__content").animate({
					scrollTop: 0
				}, 300);
			});
			
			$(document).ready(function(){
				$("#sideMenu").swipe({
					swipeRight:function(event, distance, duration, fingerCount, fingerData) {
							$(".mdl-layout__drawer").addClass("is-visible").attr("aria-hidden", "false");
							$(".mdl-layout__obfuscator").addClass("is-visible");
					}, threshold:0
				});
				$(".mdl-layout__drawer").swipe({
					swipeStatus:function(event, phase, direction, distance) {
						if(direction == "left") {						
							$(".mdl-layout__drawer.is-visible").css({"-webkit-transform": "translateX(" + (-1.5 * distance) + "px)", "transform": "translateX(" + (-1.5 * distance) + "px)"});
							if((phase == "cancel" || phase == "end") && distance > 100) {
								$(".mdl-layout__drawer").attr("aria-hidden", "true").removeClass("is-visible").attr("style", "");
								$(".mdl-layout__obfuscator").removeClass("is-visible");	
							}
							if((phase == "cancel" || phase == "end") && distance < 100) {
								$(".mdl-layout__drawer").attr("style", "");
							}
						}
					},
					allowPageScroll: "horizontal"
				});
			});
			
			$(window).on('beforeunload', function() {
				$("#loading").css("display", "table");
			});
			
			$(".mdl-layout__content").ready(function() {
				pageUpButton();
			});
			
			$(".mdl-layout__content").on("scroll", function() {
				pageUpButton();
			});
			
			function pageUpButton() {
				if($(".mdl-layout__content").scrollTop() > 500) {
					if(pageUpButton_flag == 0) {
						pageUpButton_flag = 1;
						$("#pageUpButton").show();
					}
				}
				else {
					if(pageUpButton_flag == 1) {
						$("#pageUpButton").hide();
						pageUpButton_flag = 0;
					}
				}
			}
